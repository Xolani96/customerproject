﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiddleLayer
{
    public class Lead : Customerbase
    {
        public override void Validate()
        {
            if (CustomerName.Length == 0)
            {
                throw new Exception("Customer name is required");
            }
            if (PhoneNumber.Length == 0)
            {
                throw new Exception("Customer phone number is required");
            }
            if (Address.Length == 0)
            {
                throw new Exception("Address required");
            }
        }
    }
}
