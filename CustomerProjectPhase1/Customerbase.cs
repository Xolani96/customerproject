﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiddleLayer
{
    public class Customerbase
    {
        public Customerbase()
        {
            CustomerName = "";
            PhoneNumber = "";
            Address = "";
            BillAmount = 0;
            BillDate = DateTime.Now;
        }

        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public decimal BillAmount { get; set; }
        public DateTime BillDate { get; set; }
        public virtual void Validate()
        {
            throw new Exception("Not Implemented");
        }
    }
}
