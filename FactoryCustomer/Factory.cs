﻿using System;
using System.Collections.Generic;
using MiddleLayer;

namespace FactoryCustomer
{
    public static class Factory // simple factory pattern
    {
        private static Dictionary<string, Customerbase> custs = 
            new Dictionary<string, Customerbase>();

        public static Customerbase Create(string TypeCust)
        {
            // only loaded when the create method is called and this pattern is called LAZY LOADING
            if(custs.Count == 0)
            {
                custs.Add("Customer", new Customer());
                custs.Add("Lead", new Lead());
            }

            // In order to get rid of the if statement, we used the RIP Pattern (replaces IF with POLYMORPHISM)
            return custs[TypeCust];

            // In order to get rid of the if statement, we used the RIP Pattern (replaces IF with POLYMORPHISM)
            //if (TypeCust == "Customer")
            //{
            //    return new Customer();
            //}
            //else
            //{
            //    return new Lead();
            //}
        }

    }
}
