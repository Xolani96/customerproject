USE [GUMUTS_ENG]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TABLE [dbo].[tblCust](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[BillAmount] [money] NULL,
	[BillDate] [datetime] NULL,
	[Address] [nvarchar](50) NULL,
	[CustomerType] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblCust] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO