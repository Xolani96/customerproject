﻿namespace CustomerProject
{
    partial class FrmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CustomerType_lbl = new System.Windows.Forms.Label();
            this.CustomerName_lbl = new System.Windows.Forms.Label();
            this.PhoneNumber_lbl = new System.Windows.Forms.Label();
            this.BillAmount_lbl = new System.Windows.Forms.Label();
            this.BillDate_lbl = new System.Windows.Forms.Label();
            this.Address_lbl = new System.Windows.Forms.Label();
            this.CustomerType_comboBox = new System.Windows.Forms.ComboBox();
            this.CustomerName_txtbox = new System.Windows.Forms.TextBox();
            this.BillDate_txtbox = new System.Windows.Forms.TextBox();
            this.BillAmount_txtbox = new System.Windows.Forms.TextBox();
            this.PhoneNumber_txtbox = new System.Windows.Forms.TextBox();
            this.Address_txtbox = new System.Windows.Forms.TextBox();
            this.BillAmount_comboBox = new System.Windows.Forms.ComboBox();
            this.Validate_btn = new System.Windows.Forms.Button();
            this.Cancel_btn = new System.Windows.Forms.Button();
            this.Delete_btn = new System.Windows.Forms.Button();
            this.Update_btn = new System.Windows.Forms.Button();
            this.Add_btn = new System.Windows.Forms.Button();
            this.Customer_dataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Customer_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // CustomerType_lbl
            // 
            this.CustomerType_lbl.AutoSize = true;
            this.CustomerType_lbl.Location = new System.Drawing.Point(24, 38);
            this.CustomerType_lbl.Name = "CustomerType_lbl";
            this.CustomerType_lbl.Size = new System.Drawing.Size(84, 13);
            this.CustomerType_lbl.TabIndex = 0;
            this.CustomerType_lbl.Text = "Customer Type: ";
            // 
            // CustomerName_lbl
            // 
            this.CustomerName_lbl.AutoSize = true;
            this.CustomerName_lbl.Location = new System.Drawing.Point(23, 75);
            this.CustomerName_lbl.Name = "CustomerName_lbl";
            this.CustomerName_lbl.Size = new System.Drawing.Size(85, 13);
            this.CustomerName_lbl.TabIndex = 1;
            this.CustomerName_lbl.Text = "Customer Name:";
            // 
            // PhoneNumber_lbl
            // 
            this.PhoneNumber_lbl.AutoSize = true;
            this.PhoneNumber_lbl.Location = new System.Drawing.Point(27, 112);
            this.PhoneNumber_lbl.Name = "PhoneNumber_lbl";
            this.PhoneNumber_lbl.Size = new System.Drawing.Size(81, 13);
            this.PhoneNumber_lbl.TabIndex = 2;
            this.PhoneNumber_lbl.Text = "PhoneNumber: ";
            // 
            // BillAmount_lbl
            // 
            this.BillAmount_lbl.AutoSize = true;
            this.BillAmount_lbl.Location = new System.Drawing.Point(375, 38);
            this.BillAmount_lbl.Name = "BillAmount_lbl";
            this.BillAmount_lbl.Size = new System.Drawing.Size(62, 13);
            this.BillAmount_lbl.TabIndex = 3;
            this.BillAmount_lbl.Text = "Bill Amount:";
            // 
            // BillDate_lbl
            // 
            this.BillDate_lbl.AutoSize = true;
            this.BillDate_lbl.Location = new System.Drawing.Point(388, 75);
            this.BillDate_lbl.Name = "BillDate_lbl";
            this.BillDate_lbl.Size = new System.Drawing.Size(49, 13);
            this.BillDate_lbl.TabIndex = 4;
            this.BillDate_lbl.Text = "Bill Date:";
            // 
            // Address_lbl
            // 
            this.Address_lbl.AutoSize = true;
            this.Address_lbl.Location = new System.Drawing.Point(386, 120);
            this.Address_lbl.Name = "Address_lbl";
            this.Address_lbl.Size = new System.Drawing.Size(51, 13);
            this.Address_lbl.TabIndex = 5;
            this.Address_lbl.Text = "Address: ";
            // 
            // CustomerType_comboBox
            // 
            this.CustomerType_comboBox.FormattingEnabled = true;
            this.CustomerType_comboBox.Location = new System.Drawing.Point(105, 35);
            this.CustomerType_comboBox.Name = "CustomerType_comboBox";
            this.CustomerType_comboBox.Size = new System.Drawing.Size(195, 21);
            this.CustomerType_comboBox.TabIndex = 7;
            this.CustomerType_comboBox.SelectedIndexChanged += new System.EventHandler(this.CustomerType_comboBox_SelectedIndexChanged);
            this.CustomerType_comboBox.Items.AddRange(new object[] { "Customer", "lead" });
            // 
            // CustomerName_txtbox
            // 
            this.CustomerName_txtbox.Location = new System.Drawing.Point(105, 72);
            this.CustomerName_txtbox.Name = "CustomerName_txtbox";
            this.CustomerName_txtbox.Size = new System.Drawing.Size(195, 20);
            this.CustomerName_txtbox.TabIndex = 8;
            // 
            // BillDate_txtbox
            // 
            this.BillDate_txtbox.Location = new System.Drawing.Point(468, 72);
            this.BillDate_txtbox.Name = "BillDate_txtbox";
            this.BillDate_txtbox.Size = new System.Drawing.Size(133, 20);
            this.BillDate_txtbox.TabIndex = 9;
            // 
            // BillAmount_txtbox
            // 
            this.BillAmount_txtbox.Location = new System.Drawing.Point(468, 35);
            this.BillAmount_txtbox.Name = "BillAmount_txtbox";
            this.BillAmount_txtbox.Size = new System.Drawing.Size(133, 20);
            this.BillAmount_txtbox.TabIndex = 10;
            // 
            // PhoneNumber_txtbox
            // 
            this.PhoneNumber_txtbox.Location = new System.Drawing.Point(105, 109);
            this.PhoneNumber_txtbox.Name = "PhoneNumber_txtbox";
            this.PhoneNumber_txtbox.Size = new System.Drawing.Size(195, 20);
            this.PhoneNumber_txtbox.TabIndex = 11;
            // 
            // Address_txtbox
            // 
            this.Address_txtbox.Location = new System.Drawing.Point(468, 120);
            this.Address_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.Address_txtbox.Multiline = true;
            this.Address_txtbox.Name = "Address_txtbox";
            this.Address_txtbox.Size = new System.Drawing.Size(172, 55);
            this.Address_txtbox.TabIndex = 12;
            // 
            // BillAmount_comboBox
            // 
            this.BillAmount_comboBox.FormattingEnabled = true;
            this.BillAmount_comboBox.Location = new System.Drawing.Point(621, 35);
            this.BillAmount_comboBox.Name = "BillAmount_comboBox";
            this.BillAmount_comboBox.Size = new System.Drawing.Size(119, 21);
            this.BillAmount_comboBox.TabIndex = 13;
            // 
            // Validate_btn
            // 
            this.Validate_btn.Location = new System.Drawing.Point(304, 147);
            this.Validate_btn.Margin = new System.Windows.Forms.Padding(2);
            this.Validate_btn.Name = "Validate_btn";
            this.Validate_btn.Size = new System.Drawing.Size(74, 28);
            this.Validate_btn.TabIndex = 23;
            this.Validate_btn.Text = "Validate";
            this.Validate_btn.UseVisualStyleBackColor = true;
            this.Validate_btn.Click += new System.EventHandler(this.Validate_btn_Click);
            // 
            // Cancel_btn
            // 
            this.Cancel_btn.Location = new System.Drawing.Point(231, 147);
            this.Cancel_btn.Margin = new System.Windows.Forms.Padding(2);
            this.Cancel_btn.Name = "Cancel_btn";
            this.Cancel_btn.Size = new System.Drawing.Size(74, 28);
            this.Cancel_btn.TabIndex = 22;
            this.Cancel_btn.Text = "Cancel";
            this.Cancel_btn.UseVisualStyleBackColor = true;
            // 
            // Delete_btn
            // 
            this.Delete_btn.Location = new System.Drawing.Point(163, 147);
            this.Delete_btn.Margin = new System.Windows.Forms.Padding(2);
            this.Delete_btn.Name = "Delete_btn";
            this.Delete_btn.Size = new System.Drawing.Size(69, 28);
            this.Delete_btn.TabIndex = 21;
            this.Delete_btn.Text = "Delete";
            this.Delete_btn.UseVisualStyleBackColor = true;
            // 
            // Update_btn
            // 
            this.Update_btn.Location = new System.Drawing.Point(97, 147);
            this.Update_btn.Margin = new System.Windows.Forms.Padding(2);
            this.Update_btn.Name = "Update_btn";
            this.Update_btn.Size = new System.Drawing.Size(67, 28);
            this.Update_btn.TabIndex = 20;
            this.Update_btn.Text = "Update";
            this.Update_btn.UseVisualStyleBackColor = true;
            // 
            // Add_btn
            // 
            this.Add_btn.Location = new System.Drawing.Point(30, 147);
            this.Add_btn.Margin = new System.Windows.Forms.Padding(2);
            this.Add_btn.Name = "Add_btn";
            this.Add_btn.Size = new System.Drawing.Size(66, 28);
            this.Add_btn.TabIndex = 19;
            this.Add_btn.Text = "Add";
            this.Add_btn.UseVisualStyleBackColor = true;
            // 
            // Customer_dataGridView
            // 
            this.Customer_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Customer_dataGridView.Location = new System.Drawing.Point(30, 198);
            this.Customer_dataGridView.Name = "Customer_dataGridView";
            this.Customer_dataGridView.Size = new System.Drawing.Size(710, 218);
            this.Customer_dataGridView.TabIndex = 24;
            // 
            // FrmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Customer_dataGridView);
            this.Controls.Add(this.Validate_btn);
            this.Controls.Add(this.Cancel_btn);
            this.Controls.Add(this.Delete_btn);
            this.Controls.Add(this.Update_btn);
            this.Controls.Add(this.Add_btn);
            this.Controls.Add(this.BillAmount_comboBox);
            this.Controls.Add(this.Address_txtbox);
            this.Controls.Add(this.PhoneNumber_txtbox);
            this.Controls.Add(this.BillAmount_txtbox);
            this.Controls.Add(this.BillDate_txtbox);
            this.Controls.Add(this.CustomerName_txtbox);
            this.Controls.Add(this.CustomerType_comboBox);
            this.Controls.Add(this.Address_lbl);
            this.Controls.Add(this.BillDate_lbl);
            this.Controls.Add(this.BillAmount_lbl);
            this.Controls.Add(this.PhoneNumber_lbl);
            this.Controls.Add(this.CustomerName_lbl);
            this.Controls.Add(this.CustomerType_lbl);
            this.Name = "FrmCustomer";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Customer_dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CustomerType_lbl;
        private System.Windows.Forms.Label CustomerName_lbl;
        private System.Windows.Forms.Label PhoneNumber_lbl;
        private System.Windows.Forms.Label BillAmount_lbl;
        private System.Windows.Forms.Label BillDate_lbl;
        private System.Windows.Forms.Label Address_lbl;
        private System.Windows.Forms.ComboBox CustomerType_comboBox;
        private System.Windows.Forms.TextBox CustomerName_txtbox;
        private System.Windows.Forms.TextBox BillDate_txtbox;
        private System.Windows.Forms.TextBox BillAmount_txtbox;
        private System.Windows.Forms.TextBox PhoneNumber_txtbox;
        private System.Windows.Forms.TextBox Address_txtbox;
        private System.Windows.Forms.ComboBox BillAmount_comboBox;
        private System.Windows.Forms.Button Validate_btn;
        private System.Windows.Forms.Button Cancel_btn;
        private System.Windows.Forms.Button Delete_btn;
        private System.Windows.Forms.Button Update_btn;
        private System.Windows.Forms.Button Add_btn;
        private System.Windows.Forms.DataGridView Customer_dataGridView;
    }
}

