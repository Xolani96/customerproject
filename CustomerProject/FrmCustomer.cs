﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiddleLayer;
using FactoryCustomer;

namespace CustomerProject
{
    public partial class FrmCustomer : Form
    {
        private Customerbase cust = null;
       
        public FrmCustomer()
        {
            InitializeComponent();
        }

        private void CustomerType_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            cust = Factory.Create(CustomerType_comboBox.Text);
        }

        private void SetCustomer()
        {
            cust.CustomerName = CustomerName_txtbox.Text;
            cust.PhoneNumber = PhoneNumber_txtbox.Text;
            cust.Address = Address_txtbox.Text;
            cust.BillAmount = Convert.ToDecimal(BillAmount_txtbox.Text);
            cust.BillDate = Convert.ToDateTime(BillDate_txtbox.Text);
        }

        private void Validate_btn_Click(object sender, EventArgs e)
        {
            try
            {
                SetCustomer();
                cust.Validate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
    }
}
